import requests
import os
from lxml import html
import hashlib
import time
# from bs4 import BeautifulSoup as bs
t1 = time.time()
print(t1)
DOMAIN = 'http://www.cbr.ru'
PATH = '/statistics/'


class CBRParser(object):
    def __init__(self, domain, path):
        self.req = requests
        self.html = html
        self.url = None
        self.domain = domain
        self.path = path
        self.main_links = []
        self.content = None
        self.links = []
        self.links_to_xlsx = {}

    def _get_content(self, link):
        try:
            self.content = self.req.get(link).content
        except:
            Exception("Wrong url")
        return self.content

    def find_xlsx_links(self, link):
        xpath = '//div[@id="main"]/*[not(self::ul)]/descendant::a[@href[contains(., "xlsx") or contains(., "xls")]]'
        resp = self._get_content(link)
        try:
            html_resp = html.fromstring(resp).xpath(xpath)
        except:
            Exception("Something gone wrong!")
            html_resp = False
        print('Get page: %s' % link)
        if html_resp:
            hrefs = tuple(map(lambda x: (x.text_content(),
                                         DOMAIN + PATH + x.get('href')), html_resp))
            links = dict((key, value) for key, value in hrefs)

            print("Found xlsx, urls: %s \n" % links)
            self.links_to_xlsx.update(links)
            return links
        else:
            print("Seems like page is empaty for xlsx...\n")

    def get_links(self, url=None):
        xpath = '//div[@id="main"]/*[not(self::ul)][not(self::div[@class="important-links"])]/' \
                'descendant::a[@href][not(@href[contains(., "pdf") or' \
                '                               contains(., "zip") or' \
                '                               contains(., "doc") or' \
                '                               contains(., "docx")])]'
        if url is None:
            self._get_content(self.domain + self.path)
        else:
            self._get_content(url)
        try:
            html_data = html.fromstring(self.content)
        except:
            html_data = False
            Exception("Something went wrong")
        if html_data:
            elems = html_data.xpath(xpath)
            for i in elems:
                link = self.domain + i.get('href')
                if link not in self.links and len(self.links) < 1000:
                    self.links.append(link)
                else:
                    return
        else:
            print("Cannot parse: ", url)

    def download(self, links):
        if links:
            accum = 0
            for name, value in links.items():
                response = self.req.get(value)
                # file_hash = hashlib.md5(response.content).hexdigest()
                name = name.replace(' ', '_').replace("/", '_')
                # print(name)
                filename = os.path.join('xlsx', name)
                if len(name) > 19:
                    filename = os.path.join('xlsx', name[:20])
                with open("./{}.xlsx".format(filename), 'wb') as xlsx:
                    xlsx.write(response.content)
                    accum +=1

                print("Total files: {} of {}".format(accum, len(links)))
                print("File {} is downloaded\n".format(name))
        else:
            return
    def rec(self):
        self.get_links()
        for i in self.links:
            links = self.find_xlsx_links(i)
            self.get_links(url=i)
        print(len(self.links_to_xlsx))
        print(self.links_to_xlsx)
        self.download(self.links_to_xlsx)



cbr = CBRParser(DOMAIN, PATH)
cbr.rec()
t_all = time.time() - t1
print(t_all)
    # .find_xlsx_links(
    # '//div[@id='main']/*[not(self::ul)]/descendant::a[@href[contains(., "xlsx")]]')