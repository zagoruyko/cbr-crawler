import requests
from lxml import html
from urllib.parse import urlparse
import time
t1 = time.time()
from threading import Thread
from queue import Queue

def req(url):
    try:
        r = requests.get(url)
        urls_parse = urlparse(url)
        if r.status_code == 200:
            return (r.content, urls_parse.netloc, urls_parse.path)
        else:
            print('Cannot not reach to %s' % url)
            return False
    except:
        # print('failed with %s' % url)
        return False


def get_all_links(base_url,url):
    xpath = '//div[@class="site_map"]/descendant::a[@href]'
    resp = req(base_url + url)
    data = html.fromstring(resp[0]).xpath(xpath)
    return [base_url + i.get('href') for i in data]


def download(links):
    if links:
        count = 0
        for name, link in links.items():
            if len(name) > 21:
                new_name = name[:20].replace(' ', '_').replace('/', '_')
            else:
                new_name = name.replace(' ', '_').replace('/', '_')
            content = req(link)
            if content:
                count += 1
                with open('./xlsx1/{}{}.xlsx'.format(new_name, count), 'wb') as file:
                    file.write(content[0])
                    print('File %s is downloaded\n' % new_name)
            else:
                print('error!')
            print('Complete!')
    else:
        print('Error while saving file')



def find_xlsx_links(urls):
    xpath = '//div[@id="main"]/*[not(self::ul)]/descendant::' \
            'a[@href[contains(., "xlsx") or contains(., "xls")]]'
    acc = 0
    total = {}
    for link in enumerate(urls, 1):
        print("Fetching %s. Get %s of %s" % (link[1], link[0], len(urls)))
        resp = req(link[1])
        if resp:
            xlsx = html.fromstring(resp[0]).xpath(xpath)
        else:
            print(resp)
            xlsx = False
            # return total
        if xlsx:
            acc += len(xlsx)
            url = 'http://' + resp[1] + resp[2]
            links = dict((elem.text_content(),  'http://' + resp[1] + elem.get('href')) if elem.get('href').startswith('/')
                         else (elem.text_content(),  url + elem.get('href')) for elem in xlsx)
            total.update(links)
            print("Gotcha!. Total xlsx: %s\n" % acc)
        else:
            print('Cannot found any xlsx file. Total: %s\n' % acc)

    print("RESULT: %s" % len(total))
    print(total)
    return total


links = get_all_links('http://www.cbr.ru', '/sitemap/')
xlsx = find_xlsx_links(links)
# download(xlsx)
# print(time.time() - t1)




print(time.time() - t1)