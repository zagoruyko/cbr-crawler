import requests
from lxml import html
from urllib.parse import urlparse
import time
t1 = time.time()
from threading import Thread, Lock
import threading
from queue import Queue

links_queue = Queue()
xlsx_queue = Queue()
d = {}


class Downloader(Thread):
    def __init__(self, name, links_q, xlsx_q):
        Thread.__init__(self)
        # self.daemon = True
        self.links_q = links_q
        self.xlsx_q = xlsx_q
        self.name = name
        self.lock = Lock()

    def find_xlsx_links(self, content):
        # print(content)
        xpath = '//div[@id="main"]/*[not(self::ul)]/descendant::' \
                'a[@href[contains(., "xlsx") or contains(., "xls")]]'

        xlsx_objs = html.fromstring(content).xpath(xpath)
        hrefs = dict((href.text_content()[:21], href.get('href')) for href in xlsx_objs)
        self.xlsx_q.put(hrefs)


    def fetch_url(self, url):
        try:
            url_parse = urlparse(url)
            r = requests.get(url)
            print('Fetching %s. Status code: %s' % (url, r.status_code))
            return (r.content, url_parse.netloc, url_parse.path)
        except:
            print('Failed with ', url)

    # def make_url(self, urls):
    #     url = self.xlsx_q.get()
    #     # if url is None:
    #     #     return
    #     #
    #     print(url)


    def run(self):
        a = self.xlsx_q.get()
        while True:
            item = self.links_q.get()
            if item is None:
                break
            print('THREAD #%s' % self.name)
            content = self.fetch_url(item)
            self.links_q.task_done()
            self.find_xlsx_links(content[0])
            self.xlsx_q.task_done()


def get_all_links(base_url, url):
    xpath = '//div[@class="site_map"]/descendant::a[@href]'
    resp = requests.get(base_url + url)
    data = html.fromstring(resp.content).xpath(xpath)
    return [links_queue.put(base_url + i.get('href')) for i in data]

def main():
    get_all_links('http://www.cbr.ru', '/sitemap/')
    threads = []
    t1 = time.time()
    for t in range(1, 11):
        thread = Downloader(t, links_queue, xlsx_queue)
        thread.start()
        threads.append(thread)

    xlsx_queue.join()
    links_queue.join()

    # while True:
    #     xlsx_queue.get()

    print('Complete in: ', time.time() - t1)
    print(xlsx_queue.qsize())
    return

if __name__ == "__main__":
    main()